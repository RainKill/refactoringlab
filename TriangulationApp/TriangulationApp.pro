#-------------------------------------------------
#
# Project created by QtCreator 2015-07-07T14:28:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TriangulationApp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    lib.cpp

HEADERS  += mainwindow.h \
    lib.h

CONFIG += c++11

FORMS    += mainwindow.ui
