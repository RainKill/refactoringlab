#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lib.h"
#include <QGraphicsScene>
#include <vector>
#include <QTime>
#include <QDateTime>
#include <QFile>
#include <QTextStream>
#include <QString>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
    QObject::connect(ui->circleButton, SIGNAL(clicked()), &loop, SLOT(quit()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_goButton_clicked()
{
    QString logFileName = "log.log";
    QFile log(logFileName);
    log.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&log);
    out.setCodec("UTF-8");
    QGraphicsScene *s = new QGraphicsScene(ui->graphicsView);
    std::shared_ptr<Triangle> t;
    std::shared_ptr<Vertex> v;
    s->clear();
    ui->graphicsView->setScene(s);
    int k = ui->vertexBox->value();//qrand()%10 + 21;
    out << "Vertex count: " <<k << endl;
    std::vector<Vertex> p;
    int x,y,z = 0;
    //out << "randomed" << endl;
    if(ui->radoRadioButton->isChecked())
    {
        for(int i = 0; i < k; ++i)
        {
            x = qrand() % ui->graphicsView->width();
            y = qrand() % ui->graphicsView->height();
            out << x << "|" << y << endl;
            p.push_back(Vertex(x,y,z));
        }
    }
    if(ui->fileRadioButton->isChecked())
    {
        out << "reading from file" << endl;
        QString fileName = "data";
        QString line,temp;
        QFile f(fileName);
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream in(&f);
        for(int i = 0; i < k; ++i)
        {
            line = in.readLine();
            for(int i = 0; i < line.indexOf("|"); i++)
            {
                temp += line[i];
            }
            x = temp.toInt();
            temp.clear();
            for(int i = line.indexOf("|")+1; i < line.length(); i++)
            {
                temp += line[i];
            }
            y = temp.toInt();
            temp.clear();
            out << i+1 << " " << x << "|" << y << endl;
            p.push_back(Vertex(x,y,z));
        }
    }
    log.close();
    Triangulation triang(p);
    //out << "geted" << endl;
    if(ui->drawVertexBox->isChecked())
    {
        k = triang.getVertexsCount();
        QPen penB(Qt::black);
        QBrush brushB(Qt::black);
        penB.setWidth(5);
        for(int i = 0; i < k; ++i)
        {
            v = triang.getVertex(i);
            if(v != nullptr)
            {
                // << i+1 << " x:" << x << " y: " << y << endl;
                s->addEllipse(v->x()-1,Painter::drawY(v->y(),ui->graphicsView->height())-1,2,2,penB,brushB);
                ui->graphicsView->fitInView(s->sceneRect(), Qt::KeepAspectRatio);
            }
        }
    }
    if(ui->drawTriangleBox->isChecked())
    {
        k = triang.getTriangleCount();
        QPen penR(Qt::red);
        QPen penBlue(Qt::blue);
        penR.setWidth(1);
        for(int i = 0; i < k; ++i)
        {
            t = triang.getTriangle(i);
            if(t != nullptr)
            {
                s->addLine(t->A()->x(),Painter::drawY(t->A()->y(),ui->graphicsView->height()),t->B()->x(),Painter::drawY(t->B()->y(),ui->graphicsView->height()),penR);
                s->addLine(t->B()->x(),Painter::drawY(t->B()->y(),ui->graphicsView->height()),t->C()->x(),Painter::drawY(t->C()->y(),ui->graphicsView->height()),penR);
                s->addLine(t->C()->x(),Painter::drawY(t->C()->y(),ui->graphicsView->height()),t->A()->x(),Painter::drawY(t->A()->y(),ui->graphicsView->height()),penR);
                ui->graphicsView->fitInView(s->sceneRect(), Qt::KeepAspectRatio);
            }
        }
        for(int i = 0; i < k; ++i)
        {
            t = triang.getTriangle(i);
            if(ui->circleBox->isChecked() && t && t->circumcircle().isValid())
            {
                loop.exec();
                s->addEllipse(t->circumcircle().x() - t->circumcircle().r(), Painter::drawY(t->circumcircle().y(), ui->graphicsView->height()) - t->circumcircle().r(),t->circumcircle().r() * 2, t->circumcircle().r() * 2, penBlue);
                ui->graphicsView->fitInView(s->sceneRect(), Qt::KeepAspectRatio);
            }
        }
    }
}

void MainWindow::on_circleBox_toggled(bool checked)
{
    ui->circleButton->setEnabled(checked);
}
