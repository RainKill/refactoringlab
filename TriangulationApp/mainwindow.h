#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QEventLoop>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_goButton_clicked();

    void on_circleBox_toggled(bool checked);

private:
    Ui::MainWindow *ui;
    QEventLoop loop;
};

#endif // MAINWINDOW_H
