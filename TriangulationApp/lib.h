#ifndef LIB_H
#define LIB_H

#include <vector>
#include <memory>
#include <array>

class Vertex;
class Vect;
class Triangle;
class Circle;
class Triangulation;

class Vertex
{
public:
    Vertex();
    Vertex(int x, int y, int h);
    Vertex(const Vertex& other);
    Vertex(Vertex&& other);

    ~Vertex();

    bool operator==(const Vertex &other) const;
    Vertex& operator=(const Vertex& v);
    Vertex& operator=(Vertex&& other);

    int x() const;
    int y() const;
    int h() const;

protected:
    int _x;
    int _y;
    int _h;
};

class Vect
{
protected:
    std::shared_ptr<Vertex> _begin;
    std::shared_ptr<Vertex> _end;
    int _x;
    int _y;
public:
    static int vectorProduct(const Vect& a, const Vect& b);
    static long double dotProduct(const Vect& a, const Vect& b);
    long double length() const;
    Vect(std::shared_ptr<Vertex> a, std::shared_ptr<Vertex> b);
    Vect &operator=(const Vect& v);
};

class Circle
{
public:
    Circle();
    Circle(int x, int y, int r);
    Circle(const Circle& other);
    int x() const;
    int y() const;
    int r() const;

    bool isValid() const;

    Circle &operator=(const Circle& other);
protected:
    int _x;
    int _y;
    int _r;
};

class Triangle
{
protected:
    std::shared_ptr<Vertex> _a;
    std::shared_ptr<Vertex> _b;
    std::shared_ptr<Vertex> _c;
    std::shared_ptr<Triangle> _AB;
    std::shared_ptr<Triangle> _BC;
    std::shared_ptr<Triangle> _CA;

    Circle _circumcircle;

public:
    std::shared_ptr<Vertex> A() const;
    std::shared_ptr<Vertex> B() const;
    std::shared_ptr<Vertex> C() const;
    std::shared_ptr<Triangle> AB() const;
    std::shared_ptr<Triangle> BC() const;
    std::shared_ptr<Triangle> CA() const;

    Circle circumcircle() const;

    void setAB(std::shared_ptr<Triangle> T);
    void setBC(std::shared_ptr<Triangle> T);
    void setCA(std::shared_ptr<Triangle> T);
    void setA(std::shared_ptr<Vertex> A);
    void setB(std::shared_ptr<Vertex> B);
    void setC(std::shared_ptr<Vertex> C);

    void morphInto(std::shared_ptr<Triangle> T);

    Triangle(std::shared_ptr<Vertex> A, std::shared_ptr<Vertex> B, std::shared_ptr<Vertex> C);
    Triangle();

    void findAndReplaceBuddy(std::shared_ptr<Triangle> findThis, std::shared_ptr<Triangle> replaceTo);

    int inTriangle(std::shared_ptr<Vertex> m) const;
    long double maxCos() const;

    Triangle &operator=(const Triangle &t);

private:
    void calcCircumcircle();
};

class Triangulation
{
public:
    Triangulation(const std::vector<Vertex> &vertexArray);
    Triangulation(const Triangulation& other);

    std::shared_ptr<Triangle> getTriangle(int i) const;
    std::shared_ptr<Vertex> getVertex(int i) const;
    int getTriangleCount() const;
    int getVertexsCount() const;
    int getTrianlgeIndex(std::shared_ptr<Triangle> t) const;

    bool checkDelaunayCondition(std::shared_ptr<Triangle> t1, std::shared_ptr<Triangle> t2);

    Triangulation& operator=(const Triangulation& other);

protected:
    std::vector< std::shared_ptr<Vertex> > _vertices;
    std::vector< std::shared_ptr<Triangle> > _triangles;

    std::shared_ptr<Vertex> _boundRectBottomLeft;
    std::shared_ptr<Vertex> _boundRectTopLeft;
    std::shared_ptr<Vertex> _boundRectTopRight;
    std::shared_ptr<Vertex> _boundRectBottomRight;

    void calcBoundRect();
    void createInitialTriangulation();

    void buildTriangulation();

    std::shared_ptr<Triangle> findTriangleThatContainsVertex(std::shared_ptr<Triangle> startingTriangle, std::shared_ptr<Vertex> v);
    std::array<std::shared_ptr<Triangle>, 3> splitTriangle(std::shared_ptr<Triangle> triangle, std::shared_ptr<Vertex> splitByVertex);

    void changeAllTriangleNeighbors(std::shared_ptr<Triangle> candidate, std::shared_ptr<Triangle> newNeighbor);
    void changeTriangleNeighbor(std::shared_ptr<Triangle> candidate, std::shared_ptr<Triangle> oldNeighbor, std::shared_ptr<Triangle> newNeighbor);

    std::pair<std::shared_ptr<Triangle>, std::shared_ptr<Triangle> > alternativePair(std::shared_ptr<Triangle> t1, std::shared_ptr<Triangle> t2);

    void rebuildTriangulation(std::shared_ptr<Triangle> triangleToSplit, std::array<std::shared_ptr<Triangle>, 3> arrayOfSplitted);
    void recursivePairRearrangement(std::shared_ptr<Triangle> t1, std::shared_ptr<Triangle> t2);
};

class Painter
{
public:
    static int drawY(int y, int yMax);
};

#endif // LIB_H
