#include "lib.h"

#include <algorithm>
#include <assert.h>
#include <cmath>

Vertex::Vertex()
{
    _x = 0;
    _y = 0;
    _h = 0;
}

Vertex::Vertex(int x, int y, int h)
{
    _x = x;
    _y = y;
    _h = h;
}

Vertex::Vertex(const Vertex &other)
{
    *this = other;
}

Vertex::Vertex(Vertex &&other)
{
    *this = other;
}

Vertex::~Vertex()
{

}

bool Vertex::operator==(const Vertex &other) const
{
    return _x == other._x && _y == other._y && _h == other._h;
}


Vertex &Vertex::operator=(const Vertex &v)
{
    _x = v._x;
    _y = v._y;
    _h = v._h;
    return *this;
}

Vertex &Vertex::operator=(Vertex &&other)
{
    _x = other._x;
    _y = other._y;
    _h = other._h;

    other._x = 0;
    other._y = 0;
    other._h = 0;

    return *this;
}

int Vertex::x() const
{
    return _x;
}

int Vertex::y() const
{
    return _y;
}

int Vertex::h() const
{
    return _h;
}


std::shared_ptr<Vertex> Triangle::A() const
{
    return _a;
}

std::shared_ptr<Vertex> Triangle::B() const
{
    return _b;
}

std::shared_ptr<Vertex> Triangle::C() const
{
    return _c;
}

std::shared_ptr<Triangle> Triangle::AB() const
{
    return _AB;
}

std::shared_ptr<Triangle> Triangle::BC() const
{
    return _BC;
}

std::shared_ptr<Triangle> Triangle::CA() const
{
    return _CA;
}

void Triangle::setAB(std::shared_ptr<Triangle> T)
{
    _AB = T;
}

void Triangle::setBC(std::shared_ptr<Triangle> T)
{
    _BC = T;
}

void Triangle::setCA(std::shared_ptr<Triangle> T)
{
    _CA = T;
}

void Triangle::setA(std::shared_ptr<Vertex> A)
{
    _a = A;
}

void Triangle::setB(std::shared_ptr<Vertex> B)
{
    _b = B;
}

void Triangle::setC(std::shared_ptr<Vertex> C)
{
    _c = C;
}

void Triangle::morphInto(std::shared_ptr<Triangle> T)
{
    _a = T->_a;
    _b = T->_b;
    _c = T->_c;
    _AB = T->_AB;
    _BC = T->_BC;
    _CA = T->_CA;
}

Triangle::Triangle(std::shared_ptr<Vertex> A, std::shared_ptr<Vertex> B, std::shared_ptr<Vertex> C)
{
    _a = A;
    _b = B;
    _c = C;

    calcCircumcircle();
}

Triangle::Triangle()
{
}

void Triangle::findAndReplaceBuddy(std::shared_ptr<Triangle> findThis, std::shared_ptr<Triangle> replaceTo)
{
    if(_AB == findThis)
    {
        _AB = replaceTo;
    }
    if(_BC == findThis)
    {
        _BC = replaceTo;
    }
    if(_CA == findThis)
    {
        _CA = replaceTo;
    }
}

int Triangle::inTriangle(std::shared_ptr<Vertex> m) const
{
    Vect AB(_a, _b), BC(_b, _c), CA(_c, _a), AM(_a, m), CM(_c, m), BM(_b, m);
    int AMAB, BMBC, CMCA;
    AMAB = Vect::vectorProduct(AM, AB);
    BMBC = Vect::vectorProduct(BM, BC);
    CMCA = Vect::vectorProduct(CM, CA);
    if(AMAB > 0)
        return -1;
    else if(BMBC > 0)
            return -2;
    else if(CMCA > 0)
            return -3;
    else return 0;
}

long double Triangle::maxCos() const
{
    Vect vAB(_a,_b); Vect vBA(_b,_a); Vect vBC(_b,_c);
    Vect vCB(_c,_b); Vect vAC(_a,_c); Vect vCA(_c,_a);
    long double max = 0;
    if(vAB.length() < vBC.length())
    {
        if(vAB.length() < vAC.length())
            max = Vect::dotProduct(vCB,vCA);
        else max = Vect::dotProduct(vBA,vBC);
    }
    else if (vBC.length() < vAC.length())
        max = Vect::dotProduct(vAB,vAC);
    else max = Vect::dotProduct(vBA,vBC);

    return max;
}

Circle Triangle::circumcircle() const
{
    return _circumcircle;
}

Triangle &Triangle::operator=(const Triangle &t)
{
    _a = t._a;
    _b = t._b;
    _c = t._c;
    _AB = t._AB;
    _BC = t._BC;
    _CA = t._CA;

    _circumcircle = t._circumcircle;

    return *this;
}

void Triangle::calcCircumcircle()
{
    long double D = ((_c->x() - _a->x())*(_b->y() - _a->y())) - ((_b->x() - _a->x())*(_c->y() - _a->y()));
    long double Dx = ((((pow(_c->x(),2)) - pow(_a->x(),2))/2 + ((pow(_c->y(),2)) - pow(_a->y(),2))/2)*(_b->y() - _a->y())) - (((pow(_b->x(),2) - pow(_a->x(),2))/2 + (pow(_b->y(),2)-pow(_a->y(),2))/2)*(_c->y() - _a->y()));
    long double Dy = ((_c->x() - _a->x())*((pow(_b->x(),2)-pow(_a->x(),2))/2 + (pow(_b->y(),2)-pow(_a->y(),2))/2))-((_b->x() - _a->x())*(((pow(_c->x(),2)) - pow(_a->x(),2))/2 + ((pow(_c->y(),2)) - pow(_a->y(),2))/2));

    int x = round(Dx/D);
    int y = round(Dy/D);

    std::shared_ptr<Vertex> O(new Vertex(x, y, 0));
    Vect r(_a, O);

    int rad = r.length();
    _circumcircle = Circle(x,y,rad);
}

Triangulation::Triangulation(const Triangulation &other)
{
    *this = other;
}

std::shared_ptr<Triangle> Triangulation::getTriangle(int i) const
{
    assert(i >= 0 && i < _triangles.size());
    return _triangles[i];
}

std::shared_ptr<Vertex> Triangulation::getVertex(int i) const
{
    assert(i >= 0 && i < _vertices.size());
    return _vertices[i];
}

int Triangulation::getTriangleCount() const
{
    return _triangles.size();
}

int Triangulation::getVertexsCount() const
{
    return _vertices.size();
}

int Triangulation::getTrianlgeIndex(std::shared_ptr<Triangle> t) const
{
    auto it = std::find(_triangles.begin(), _triangles.end(), t);
    assert(it != _triangles.end());
    return std::distance(_triangles.begin(), it);
}

bool Triangulation::checkDelaunayCondition(std::shared_ptr<Triangle> t1, std::shared_ptr<Triangle> t2)
{
    int x0, y0;
    int x1 = t1->A()->x(); int y1 = t1->A()->y();
    int x2 = t1->C()->x(); int y2 = t1->C()->y();
    int x3 = t1->B()->x(); int y3 = t1->B()->y();

    if(t2->AB() == t1)
    {
        x0 = t2->C()->x(); y0 = t2->C()->y();
    }
    else if(t2->BC() == t1)
    {
        x0 = t2->A()->x(); y0 = t2->A()->y();
    }
    else if(t2->CA() == t1)
    {
        x0 = t2->B()->x(); y0 = t2->B()->y();
    }
    else
    {
        //недостежимо, если треугольники строятся верно
        assert(false);
    }

    long long sp1 = (x2-x1)*(x2-x3)+(y2-y1)*(y2-y3),
            sp2 = (x0-x1)*(x0-x3)+(y0-y1)*(y0-y3);

    if(sp1 >= 0 && sp2 >= 0)
    {
        return true;
    }
    else if(sp1 < 0 && sp2 < 0)
    {
        return false;
    }

    long long vp1 = abs((x0 - x1) * (y0 - y3) - (x0 - x3) * (y0 - y1));
    long long vp2 = abs((x2 - x1) * (y2 - y3) - (x2 - x3) * (y2 - y1));
    long long sum = (vp1 * sp1) + (sp2 * vp2);

    return sum >= 0;
}

void Triangulation::buildTriangulation()
{
    for(std::shared_ptr<Vertex> curVertex : _vertices)
    {
        std::shared_ptr<Triangle> t = findTriangleThatContainsVertex(_triangles[0], curVertex);

        std::array<std::shared_ptr<Triangle>, 3> splitted = splitTriangle(t, curVertex);

        rebuildTriangulation(t, splitted);
    }
}

Triangulation &Triangulation::operator=(const Triangulation &other)
{
    _vertices = other._vertices;
    _triangles = other._triangles;

    _boundRectBottomLeft = other._boundRectBottomLeft;
    _boundRectBottomRight = other._boundRectBottomRight;
    _boundRectTopLeft = other._boundRectTopLeft;
    _boundRectTopRight = other._boundRectTopRight;

    return *this;
}

Triangulation::Triangulation(const std::vector<Vertex> &vertexArray)
{
    for(const Vertex& v : vertexArray)
    {
        _vertices.push_back(std::make_shared<Vertex>(v));
    }

    calcBoundRect();

    createInitialTriangulation();

    buildTriangulation();
}

void Triangulation::calcBoundRect()
{
    int minX = _vertices[0]->x();
    int minY = _vertices[0]->y();

    int maxX = _vertices[0]->x();
    int maxY = _vertices[0]->y();

    for(auto v : _vertices)
    {
        if(v->x() > maxX)
        {
            maxX = v->x();
        }
        else if(v->x() < minX)
        {
            minX = v->x();
        }

        if(v->y() > maxY)
        {
            maxY = v->y();
        }
        else if(v->y() < minY)
        {
            minY = v->y();
        }
    }

    const int margin = 50;

    _boundRectBottomLeft = std::shared_ptr<Vertex>(new Vertex(minX - margin, minY - margin, 0));
    _boundRectTopLeft = std::shared_ptr<Vertex>(new Vertex(minX - margin, maxY + margin, 0));
    _boundRectTopRight = std::shared_ptr<Vertex>(new Vertex(maxX + margin, maxY + margin, 0));
    _boundRectBottomRight = std::shared_ptr<Vertex>(new Vertex(maxX + margin, minY - margin, 0));
}

void Triangulation::createInitialTriangulation()
{
    _triangles.push_back(std::shared_ptr<Triangle>(new Triangle(_boundRectBottomRight, _boundRectTopRight, _boundRectTopLeft)));
    _triangles.push_back(std::shared_ptr<Triangle>(new Triangle(_boundRectBottomRight, _boundRectTopLeft, _boundRectBottomLeft)));

    _triangles[0]->setCA(_triangles[1]);
    _triangles[1]->setAB(_triangles[0]);
}

std::shared_ptr<Triangle> Triangulation::findTriangleThatContainsVertex(std::shared_ptr<Triangle> startingTriangle, std::shared_ptr<Vertex> v)
{
    std::shared_ptr<Triangle> t = startingTriangle;
    int j = t->inTriangle(v);

    switch (j)
    {
        case -1:
            return findTriangleThatContainsVertex(t->AB(), v);
            break;
        case -2:
            return findTriangleThatContainsVertex(t->BC(), v);
            break;
        case -3:
            return findTriangleThatContainsVertex(t->CA(), v);
            break;
        default:
            break;
    }

    return t;
}

std::array<std::shared_ptr<Triangle>, 3> Triangulation::splitTriangle(std::shared_ptr<Triangle> triangle, std::shared_ptr<Vertex> splitByVertex)
{
    std::shared_ptr<Triangle> b1 = std::make_shared<Triangle>(Triangle(triangle->A(), triangle->B(), splitByVertex));
    std::shared_ptr<Triangle> b2 = std::make_shared<Triangle>(Triangle(triangle->B(), triangle->C(), splitByVertex));
    std::shared_ptr<Triangle> b3 = std::make_shared<Triangle>(Triangle(triangle->C(), triangle->A(), splitByVertex));

    b1->setAB(triangle->AB()); b1->setBC(b2); b1->setCA(b3);
    b2->setAB(triangle->BC()); b2->setBC(b3); b2->setCA(b1);
    b3->setAB(triangle->CA()); b3->setBC(b1); b3->setCA(b2);

    return { b1, b2, b3 };
}

void Triangulation::changeAllTriangleNeighbors(std::shared_ptr<Triangle> candidate, std::shared_ptr<Triangle> newNeighbor)
{
    if(candidate->AB())
    {
        candidate->AB()->findAndReplaceBuddy(candidate, newNeighbor);
    }
    if(candidate->BC())
    {
        candidate->BC()->findAndReplaceBuddy(candidate, newNeighbor);
    }
    if(candidate->CA())
    {
        candidate->CA()->findAndReplaceBuddy(candidate, newNeighbor);
    }
}

void Triangulation::changeTriangleNeighbor(std::shared_ptr<Triangle> candidate, std::shared_ptr<Triangle> oldNeighbor, std::shared_ptr<Triangle> newNeighbor)
{
    assert(oldNeighbor && newNeighbor);
    if(candidate)
    {
        candidate->findAndReplaceBuddy(oldNeighbor, newNeighbor);
    }
}

std::pair<std::shared_ptr<Triangle>, std::shared_ptr<Triangle> > Triangulation::alternativePair(std::shared_ptr<Triangle> t1, std::shared_ptr<Triangle> t2)
{
    std::shared_ptr<Triangle> b1, b2;

    if(t2->AB() == t1)
    {
        b1 = std::make_shared<Triangle>(Triangle(t2->C(), t1->B(), t1->C()));
        b2 = std::make_shared<Triangle>(Triangle(t1->A(), t2->C(), t1->C()));

        b1->setAB(t2->CA()); b1->setBC(t1->BC()); b1->setCA(b2);
        b2->setAB(t2->BC()); b2->setBC(b1); b2->setCA(t1->CA());
    }
    else if(t2->BC() == t1)
    {
        b1 = std::make_shared<Triangle>(Triangle(t2->A(), t1->B(), t1->C()));
        b2 = std::make_shared<Triangle>(Triangle(t1->A(), t2->A(), t1->C()));

        b1->setAB(t2->AB()); b1->setBC(t1->BC()); b1->setCA(b2);
        b2->setAB(t2->CA()); b2->setBC(b1); b2->setCA(t1->CA());
    }
    else if(t2->CA() == t1)
    {
        b1 = std::make_shared<Triangle>(Triangle(t2->B(), t1->B(), t1->C()));
        b2 = std::make_shared<Triangle>(Triangle(t1->A(), t2->B(), t1->C()));

        b1->setAB(t2->BC()); b1->setBC(t1->BC()); b1->setCA(b2);
        b2->setAB(t2->AB()); b2->setBC(b1); b2->setCA(t1->CA());
    }

    return std::make_pair(b1, b2);
}

void Triangulation::rebuildTriangulation(std::shared_ptr<Triangle> triangleToSplit, std::array<std::shared_ptr<Triangle>, 3> arrayOfSplitted)
{
    _triangles[getTrianlgeIndex(triangleToSplit)] = arrayOfSplitted[0];
    _triangles.push_back(arrayOfSplitted[1]);
    _triangles.push_back(arrayOfSplitted[2]);

    changeTriangleNeighbor(triangleToSplit->AB(), triangleToSplit, arrayOfSplitted[0]);
    changeTriangleNeighbor(triangleToSplit->BC(), triangleToSplit, arrayOfSplitted[1]);
    changeTriangleNeighbor(triangleToSplit->CA(), triangleToSplit, arrayOfSplitted[2]);

    if(arrayOfSplitted[0]->AB())
    {
        recursivePairRearrangement(arrayOfSplitted[0], arrayOfSplitted[0]->AB());
    }
    if(arrayOfSplitted[1]->AB())
    {
        recursivePairRearrangement(arrayOfSplitted[1], arrayOfSplitted[1]->AB());
    }
    if(arrayOfSplitted[2]->AB())
    {
        recursivePairRearrangement(arrayOfSplitted[2], arrayOfSplitted[2]->AB());
    }
}

void Triangulation::recursivePairRearrangement(std::shared_ptr<Triangle> t1, std::shared_ptr<Triangle> t2)
{
    if(!checkDelaunayCondition(t1, t2))
    {
        auto altPair = alternativePair(t1, t2);

        changeAllTriangleNeighbors(t1, altPair.first);
        changeAllTriangleNeighbors(t2, altPair.second);


        _triangles[getTrianlgeIndex(t1)] = altPair.first;
        _triangles[getTrianlgeIndex(t2)] = altPair.second;

        if(altPair.first->AB())
        {
            recursivePairRearrangement(altPair.first, altPair.first->AB());
        }
        if(altPair.second->AB())
        {
            recursivePairRearrangement(altPair.second, altPair.second->AB());
        }
    }
}

int Vect::vectorProduct(const Vect& a, const Vect& b)
{
    return ((a._x * b._y) - (b._x * a._y));
}

long double Vect::dotProduct(const Vect &a, const Vect &b)
{
    long double i,j,k;
    i = (a._x * b._x) + (a._y * b._y);
    j = a.length() * b.length();
    k = i/j;
    return k;
}

long double Vect::length() const
{
    return sqrt(pow(_x,2) + pow(_y,2));
}

Vect::Vect(std::shared_ptr<Vertex> a, std::shared_ptr<Vertex> b)
{
    _x = b->x() - a->x();
    _y = b->y() - a->y();
    _begin = a;
    _end = b;
}

Vect &Vect::operator=(const Vect &v)
{
    _begin = v._begin;
    _end = v._end;
    _x = v._x;
    _y = v._y;

    return *this;
}


int Painter::drawY(int y, int yMax)
{
    return round(((y*yMax)/(-yMax))+yMax);
}


Circle::Circle()
{
    _x = 0;
    _y = 0;
    _r = 0;
}

Circle::Circle(int x, int y, int r)
{
    _x = x;
    _y = y;
    _r = r;
}

Circle::Circle(const Circle &other)
{
    *this = other;
}

int Circle::x() const
{
    return _x;
}

int Circle::y() const
{
    return _y;
}

int Circle::r() const
{
    return _r;
}

bool Circle::isValid() const
{
    return _r != 0;
}

Circle &Circle::operator=(const Circle &other)
{
    _x = other._x;
    _y = other._y;
    _r = other._r;

    return *this;
}
