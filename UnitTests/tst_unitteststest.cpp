#include <QString>
#include <QtTest>

#include "../TriangulationApp/lib.h"

//using namespace s;

class UnitTestsTest : public QObject
{
    Q_OBJECT

public:
    UnitTestsTest();

private Q_SLOTS:
    void testVertexCreationCorrectness();
    void testTriangleNeighborhood();
    void testTriangulation();
};

UnitTestsTest::UnitTestsTest()
{
}

void UnitTestsTest::testVertexCreationCorrectness()
{
    Vertex v(1, 2, 3);

    QVERIFY2(v.x() == 1, "v.x() != x");
    QVERIFY2(v.y() == 2, "v.y() != y");
    QVERIFY2(v.h() == 3, "v.h() != h");
}

void UnitTestsTest::testTriangleNeighborhood()
{
    std::shared_ptr<Vertex> xy00(new Vertex(0, 0 ,0));
    std::shared_ptr<Vertex> xy40(new Vertex(4, 0, 0));
    std::shared_ptr<Vertex> xy23(new Vertex(2, 3, 0));
    std::shared_ptr<Vertex> xy64(new Vertex(6, 4, 0));
    std::shared_ptr<Vertex> xy26(new Vertex(2, 6, 0));

    std::shared_ptr<Triangle> t1(new Triangle(xy00, xy40, xy23));
    std::shared_ptr<Triangle> t2(new Triangle(xy40, xy23, xy64));
    std::shared_ptr<Triangle> t3(new Triangle(xy23, xy64, xy26));
    std::shared_ptr<Triangle> t4(new Triangle(xy00, xy23, xy26));

    t1->setBC(t2);
    t1->setCA(t4);

    t2->setAB(t1);
    t2->setBC(t3);

    t3->setAB(t2);
    t3->setCA(t4);

    t4->setAB(t3);
    t4->setCA(t1);

    QVERIFY2(t1->BC() == t2, "t1.BC() != t2");
    QVERIFY2(t1->CA() == t4, "t1.CA() != t4");

    QVERIFY2(t2->AB() == t1, "t2.AB() != t1");
    QVERIFY2(t2->BC() == t3, "t2.BC() != t3");

    QVERIFY2(t3->AB() == t2, "t3.AB() != t2");
    QVERIFY2(t3->CA() == t4, "t3.CA() != t4");

    QVERIFY2(t4->AB() == t3, "t4.AB() != t3");
    QVERIFY2(t4->CA() == t1, "t4.CA() != t1");
}

void UnitTestsTest::testTriangulation()
{
    Vertex v1(374, 160, 0);
    Vertex v2(488, 385, 0);
    Vertex v3(94, 171, 0);
    Vertex v4(303, 167, 0);
    Vertex v5(357, 215, 0);

    std::vector<Vertex> srf { v1, v2, v3, v4, v5 };

    Triangulation s(srf);

    int n = s.getTriangleCount();

    for(int i = 0; i < n; ++i)
    {
        std::shared_ptr<Triangle> t = s.getTriangle(i);
        if(t->AB())
        {
            QVERIFY2(s.checkDelaunayCondition(t, t->AB()), "Don't meet the condition");
        }
        if(t->BC())
        {
            QVERIFY2(s.checkDelaunayCondition(t, t->BC()), "Don't meet the condition");
        }
        if(t->CA())
        {
            QVERIFY2(s.checkDelaunayCondition(t, t->CA()), "Don't meet the condition");
        }
    }
}

QTEST_APPLESS_MAIN(UnitTestsTest)

#include "tst_unitteststest.moc"
